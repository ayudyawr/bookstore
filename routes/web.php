<?php


use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\GenreController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\BookController;
use App\Http\Controllers\BorrowController;
use App\Http\Controllers\ProfileController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [HomeController::class, 'index']);
Auth::routes();

// Route::get('/book', [BookController::class, 'index']);

// Route::get('/book/create', [BookController::class, 'create']);

// Route::post('/book', [BookController::class, 'store']);

// Route::post('/book/{book_id}', [BookController::class, 'show']);

// Route::post('/book/{book_id}/edit', [BookController::class, 'edit']);

// Route::put('/book/{book_id}', [BookController::class, 'update']);

// Route::delete('/book/{book_id}', [BookController::class, 'destroy']);

Route::resource('dashboard', DashboardController::class);

// Route::middleware(['auth'])->group(function () {

// });

// Route::middleware(['auth', 'admin'])->group(function () {
//     Route::resource('genre', GenreController::class);
//     Route::resource('book', BookController::class);
// });

Route::group(['middleware' => ['auth', 'CekRole:user']], function () {
    Route::post('/dashboard/{book_id}', [DashboardController::class, 'tambah']);
    Route::get('/borrow', [BorrowController::class, 'index']);
    Route::delete('/borrow/{book_id}', [BorrowController::class, 'destroy']);
});

Route::group(['middleware' => ['auth', 'CekRole:admin']], function () {
    Route::resource('genre', GenreController::class);
    Route::resource('book', BookController::class);
});

Route::resource('profile', ProfileController::class)->middleware('auth');
