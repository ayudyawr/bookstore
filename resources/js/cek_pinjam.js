$('.borrow-button').click(function() {
    var bookId = $(this).attr('id');
    // Make an AJAX call to your Laravel controller
    // to update the status of the book in the database
    $.ajax({
        url: '/books/' + bookId + '/borrow',
        type: 'PUT',
        success: function(response) {
            if (response.borrowed) {
                $('#' + bookId).text('Return');
            } else {
                $('#' + bookId).text('Borrow');
            }
        }
    });
});