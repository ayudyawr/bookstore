@extends('layouts.master')

@section('content')
<div class="container-fluid">
    <h4 class="card-title">New Book Form</h4>
    <div class="card">
        <div class="card-body">
            <form action="/book" method="POST" class="forms-sample">
            @csrf
            <div class="form-group">
                <label for="title">Title</label>
                <input type="text" name="title" class="form-control" id="title" placeholder="Title">
            </div>
            @error('title')
                <div class="alert alert-danger">
                    {{$message}}
                </div>
            @enderror
            <div class="form-group">
                <label for="writer">Writer</label>
                <input type="text" name="writer" class="form-control" id="writer" placeholder="Writer">
            </div>
            @error('writer')
                <div class="alert alert-danger">
                    {{$message}}
                </div>
            @enderror
            <div class="form-group">
                <label for="publisher">Publisher</label>
                <input type="text" name="publisher" class="form-control" id="publisher" placeholder="Publisher">
            </div>
            @error('publisher')
                <div class="alert alert-danger">
                    {{$message}}
                </div>
            @enderror
            <div class="form-group">
                <label for="genre-id">Genre:</label>
                <select name="genre_id" id="genre_id" class="form-control">
                    <option>--Pilih Genre--</option>
                    @forelse ($genre as $item)
                        <option value="{{$item->id}}">{{$item->name}}</option>
                    @empty
                        <option>Tidak ada genre</option>
                    @endforelse
                </select>
            </div>
            @error('genre_id')
                <div class="alert alert-danger">
                    {{$message}}
                </div>
            @enderror
            <div class="form-group">
                <label for="year">Year</label>
                <input type="number" name="year" class="form-control" id="year" placeholder="year">
            </div>
            @error('year')
                <div class="alert alert-danger">
                    {{$message}}
                </div>
            @enderror
            <div class="form-group">
                <label for="synopsis">Synopsis</label>
                <textarea name="synopsis" id="synopsis" class="form-control" id="synopsis" placeholder="synopsis" rows="5"></textarea>
            </div>
            @error('synopsis')
                <div class="alert alert-danger">
                    {{$message}}
                </div>
            @enderror
            <button type="submit" class="btn btn-primary mr-2" onclick="tampil()">Submit</button>
            <button class="btn btn-light">Cancel</button>
            
            </form>
        </div>
    </div>
</div>

@endsection
@push('scripts')
    <script>
        function tampil(){
            swal("Berhasil!", "Buku baru dimasukkan!", "success");
        }
        
    </script>
@endpush