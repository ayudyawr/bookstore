@extends('layouts.master')

@section('content')
   <div class="container">
    <div class="card">
        <div class="card-body">
            <h1 class="text-primary">{{$books->title}} ({{$books->year}})</h1>
            <h5>By: {{$books->writer}}</h5>
            <h6>Publisher: {{$books->publisher}}</h6>
            <h6>Genre: {{$books->genre->name}}</h6>
            <p>{{$books->synopsis}}</p>
        </div>
    </div>
   </div>
@endsection