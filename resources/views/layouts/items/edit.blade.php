@extends('layouts.master')

@section('content')
<div class="container">
    <h4 class="card-title">Edit Book Form</h4>
    <div class="card">
        <div class="card-body">
            <form action="/book/{{$books->id}}" method="POST" class="forms-sample" >
                @csrf
                @method('PUT')
                <div class="form-group">
                    <label for="title">Title</label> 
                    <input type="text" name="title" class="form-control" id="title" placeholder="Title" value={{$books->title}}>
                </div>
                @error('title')
                <div class="alert alert-danger">
                   {{$message}}
                </div>
              @enderror
                <div class="form-group">
                    <label for="writer">Writer</label>
                    <input type="text" name="writer" class="form-control" id="writer" placeholder="writer" value={{$books->writer}}>
                </div>
                @error('writer')
                <div class="alert alert-danger">
                   {{$message}}
                </div>
              @enderror
                <div class="form-group">
                    <label for="genre_id">Genre:</label>
                    <select name="genre_id" class="form-control" >
                        <option value={{$books->genre_id}}>{{$books->genre->name}}</option>
                        @forelse ($genre as $item)
                            <option value={{$item->id}}>{{$item->name}}</option>
                        @empty
                            <option value="">Tidak ada genre</option>
                        @endforelse
                    </select>
                </div>
                @error('genre_id')
                <div class="alert alert-danger">
                   {{$message}}
                </div>
              @enderror
                <div class="form-group">
                    <label for="publisher">Publisher</label>
                    <input type="text" name="publisher" class="form-control" id="publisher" placeholder="publisher" value={{$books->publisher}}>
                </div>
                @error('publisher')
                <div class="alert alert-danger">
                   {{$message}}
                </div>
              @enderror
                <div class="form-group">
                    <label for="year">Year</label>
                    <input type="number" name="year" value={{$books->year}} class="form-control" id="year" placeholder="year" value={{$books->year}}>
                </div>
                @error('year')
                <div class="alert alert-danger">
                   {{$message}}
                </div>
              @enderror
                <div class="form-group">
                    <label for="synopsis">synopsis</label>
                    <textarea name="synopsis" id="synopsis" rows="5" class="form-control" id="synopsis" placeholder="synopsis">{{$books->synopsis}}</textarea>
                </div>
                @error('synopsis')
                <div class="alert alert-danger">
                   {{$message}}
                </div>
              @enderror
                <button type="submit" class="btn btn-primary mr-2"> Submit </button>
                <button class="btn btn-light">Cancel</button>
            </form>
        </div>
    </div>
</div>
@endsection