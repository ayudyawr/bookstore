@extends('layouts.master')

@section('content')
<div class="container-fluid">
    <h4 class="card-title">Borrow Table</h4>
</div>
<div class="col-lg-12 grid-margin stretch-card">
    <div class="card">
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Title</th>
                            <th>Writer</th>
                            <th>Year</th>
                        </tr>
                    </thead>
                    <tbody>
                        @forelse ($borrowedBooks as $item => $borrow)
                        <tr>
                            <td>{{$item + 1}}</td>
                            <td>{{$borrow->book->title}}</td>
                            <td>{{$borrow->book->writer}}</td>
                            <td>{{$borrow->book->year}}</td>
                            <td class="d-flex">
                                <form action="/borrow/{{$borrow->id}}" method="POST">
                                    @csrf
                                    @method('DELETE')
                                    <input type="submit" value="Kembalikan" class="btn btn-danger">
                                </form>
                            </td>
                        </tr>
                        @empty
                        <tr>
                            <td colspan="4" align="center">No Borrow Book</td>
                        </tr>    
                        @endforelse
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection