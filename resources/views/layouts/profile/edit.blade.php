@extends('layouts.master')
@section('content')
    <div class="container">
        <h4 class="card-title">Profile</h4>
        <div class="card">
            <div class="card-body">
                <form action="/profile/{{$profile->id}}" method="POST" class="forms-sample">
                    @csrf
                    @method('PUT')
                    {{-- <div class="form-group">
                        <label for="name">Name</label>
                        <input type="text" name="name" value={{$profile->user->name}} class="form-control" id="name" placeholder="Name">
                    </div>
                    @error('name')
                        <div class="alert alert-danger">
                            {{$message}}
                        </div>
                    @enderror
                    <div class="form-group">
                        <label for="email">Email</label>
                        <input type="email" name="email" value={{$profile->user->email}} class="form-control" id="email" placeholder="Email">
                    </div>
                    @error('email')
                        <div class="alert alert-danger">
                            {{$message}}
                        </div>
                    @enderror
                    <div class="form-group">
                        <label for="password">Password</label>
                        <input type="password" name="password" value={{$profile->user->password}} class="form-control" id="password" placeholder="Password">
                    </div>
                    @error('password')
                        <div class="alert alert-danger">
                            {{$message}}
                        </div>
                    @enderror --}}
                    <div class="form-group">
                        <label for="age">Age</label>
                        <input id="age" type="number" name="age" value={{$profile->age}} class="form-control"  placeholder="Age">
                    </div>
                    @error('age')
                        <div class="alert alert-danger">
                            {{$message}}
                        </div>
                    @enderror
                    <div class="form-group">
                        <label for="phone_number">Phone Number</label>
                        <input id="phone_number" type="text" name="phone_number" value={{$profile->phone_number}} class="form-control" placeholder="Phone Number">
                    </div>
                    @error('phone_number')
                        <div class="alert alert-danger">
                            {{$message}}
                        </div>
                    @enderror
                    <div class="form-group">
                        <label for="address">Address</label>
                        <textarea name="address" id="address" class="form-control">{{$profile->address}}</textarea>
                    </div>
                    @error('address')
                        <div class="alert alert-danger">
                            {{$message}}
                        </div>
                    @enderror
                    
                    <button class="btn btn-primary mr-2">Submit</button>
                    <button class="btn light">Cancel</button>
                </form>
            </div>
        </div>
    </div>
@endsection