<nav class="sidebar sidebar-offcanvas" id="sidebar">
    <ul class="nav">
        <li class="nav-item nav-profile">
            <a href="#" class="nav-link">
            <div class="nav-profile-image">
                <img src="{{asset('assets/images/faces/face1.jpg')}}" alt="profile" />
                <span class="login-status online"></span>
                <!--change to offline or busy as needed-->
            </div>
            <div class="nav-profile-text d-flex flex-column pr-3">
                <span class="font-weight-medium mb-0">
                    @guest
                        Guest
                    @endguest
                    @auth
                    {{Auth::user()->name}}</span>
                        
                    @endauth
            </div>
            {{-- <span class="badge badge-danger text-white ml-3 rounded">3</span> --}}
            </a>
        </li>
            <li class="nav-item">
                <a class="nav-link" href="/dashboard">
                    <i class="mdi mdi-home menu-icon"></i>
                    <span class="menu-title">Dashboard</span>
                </a>
            </li>
            @auth
                
            @if (auth()->user()->role=='admin')
                <li class="nav-item">
                    <a class="nav-link" href="/book">
                        <i class="mdi mdi-home menu-icon"></i>
                        <span class="menu-title">CRUD Buku</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="/genre">
                        <i class="mdi mdi-home menu-icon"></i>
                        <span class="menu-title">CRUD Genre</span>
                    </a>
                </li>
            @endif
            @if (auth()->user()->role=='user')
            <li class="nav-item">
                <a class="nav-link" href="/borrow">
                    <i class="mdi mdi-home menu-icon"></i>
                    <span class="menu-title">Buku yang dipinjam</span>
                </a>
            </li>
            @endif
            @endauth
                
           

            @guest
            <li class="nav-item" bg-info>
                <a class="nav-link" href="/login">
            
                    Login
                </a>

                <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                    @csrf
                </form>
            </li>
            @endguest

            @auth
            <li class="nav-item">
                <a class="nav-link" href="/profile/{{Auth::user()->id}}/edit">
                    <i class="mdi mdi-home menu-icon"></i>
                    <span class="menu-title">Profile</span>
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="{{ route('logout') }}"
                   onclick="event.preventDefault();
                                 document.getElementById('logout-form').submit();">
                    Logout
                </a>

                <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                    @csrf
                </form>
            </li>
                
            @endauth


            {{-- <li class="nav-item">
                <a class="nav-link" href="/">
                    <i class="mdi mdi-home menu-icon"></i>
                    <span class="menu-title">Dashboard</span>
                </a>
            </li> --}}
        </ul>
</nav>