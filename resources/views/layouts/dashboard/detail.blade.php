@extends('layouts.master')

@section('content')
    <div class="container">
        <div class="card">
            <div class="card-body">
                <form action="/dashboard/{{$books->id}}" method="POST">
                    @csrf
                    <h1 class="text-primary">{{$books->title}} ({{$books->year}})</h1>
                    <h5>By: {{$books->writer}}</h5>
                    <h6>Publisher: {{$books->publisher}}</h6>
                    <h6>Genre: {{$books->genre->name}}</h6>
                    <p>{{$books->synopsis}}</p>
                    <button type="submit" class="btn btn-primary mr-2" id="submit">Pinjam</button>
                </form>
            </div>
        </div>
    </div>
@endsection