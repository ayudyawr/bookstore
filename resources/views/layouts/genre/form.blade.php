@extends('layouts.master')
@section('content')
<div class="container-fluid">
    <h4 class="card-title">New Genre</h4>
    <div class="card">
        <div class="card-body">
            <form action="/genre" method="POST" class="forms-sample">
            @csrf
            <div class="form-group">
                <label for="name">Name:</label>
                <input type="name" name="name" class="form-control" id="name" placeholder="Name">
            </div>
            @error('name')
                <div class="alert alert-danger">
                    {{$message}}
                </div>
            @enderror
            <div class="form-group">
                <label for="description">Description:</label>
                <textarea name="description" id="description" class="form-control" id="description" placeholder="description" rows="5"></textarea>
            </div>
            @error('description')
                <div class="alert alert-danger">
                    {{$message}}
                </div>
            @enderror
            <button type="submit" class="btn btn-primary mr-2">Submit</button>
            <button class="btn btn-light">Cancel</button>
            </form>
        </div>
    </div>
</div>
@endsection