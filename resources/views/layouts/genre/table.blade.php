@extends('layouts.master')
@section('content')
    
<div class="container-fluid">
    <h4 class="card-title">Genre Table</h4>
</div>
<div class="col-lg-12 grid-margin stretch-card">
    <div class="card">
        <div class="card-body">
            <div class="table-responsive">
                <a href="/genre/create">
                    <button class="btn btn-primary mb-3">Create New Genre</button>
                </a>
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Name</th>
                        </tr>
                    </thead>
                    <tbody>
                        @forelse ($genres as $item => $genre)
                        <tr>
                            <td>{{$item + 1}}</td>
                            <td>{{$genre->name}}</td>
                            <td class="d-flex">
                                <a href="/genre/{{$genre->id}}" class="btn btn-info btn-sm mr-2">detail</a>
                                <a href="/genre/{{$genre->id}}/edit" class="btn btn-default btn-sm mr-2">edit</a>
                                <form action="/genre/{{$genre->id}}" method="POST">
                                    @csrf
                                    @method('DELETE')
                                    <input type="submit" value="delete" class="btn btn-danger">
                                </form>
                            </td>
                        </tr>
                        @empty
                        <tr>
                            <td colspan="4" align="center">No Genre</td>
                        </tr>    
                        @endforelse
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection