# Final Project

## Kelompok 1

-   Rachma Ayudyawati (@ayudyawr)
-   Pierra M Shobr (@pierrams1 )
-   Paul Titto (@ptitto)

## Tema Project

Sistem Pinjam Buku (perpustakaan)

## ERD

![image.png](https://gitlab.com/ayudyawr/bookstore/-/raw/main/public/bookstore.png)

## Link Deploy

Link Deploy : (https://bookstorekelompok1.sanbercodeapp.com/)
