<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Buku;
use App\Models\Borrow;
use Illuminate\Support\Facades\Auth;

class DashboardController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $books = Buku::all();
        return view('layouts.dashboard.dashboard', compact('books'));
    }

    public function tambah(Request $request, $id)
    {
        $user_id = Auth::id();

        // $cek_buku = Borrow::where('book_id', $id)->where('user_id', $user_id)->first();
        $borrow = Borrow::create([
            'user_id' => $user_id,
            'book_id' => $id
        ]);

        return redirect('/dashboard');
    }

    // public function cek_buku(Request $request, $id)
    // {
    //     $cek_buku = Borrow::where('book_id', $id)->where('user_id', Auth::user()->id)->first();
    //     if ($cek_buku) {
    //         $cek_buku->delete();
    //         return response()->json(['borrowed' => false]);
    //     } else {
    //         Borrow::create([
    //             'date_borrow' => date("2023-01-20"),
    //             'date_return' => date("2023-01-23"),
    //             'user_id' => Auth::user()->id,
    //             'book_id' => $id
    //         ]);
    //     }
    //     return response()->json(['borrowed' => true]);
    // }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $books = Buku::find($id);
        return view('layouts.dashboard.detail', compact('books'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
