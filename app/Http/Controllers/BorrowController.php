<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use App\Models\Borrow;
use Illuminate\Support\Facades\Auth;

class BorrowController extends Controller
{
    //
    public function index()
    {
        $user_id = Auth::id();
        $borrowedBooks = Borrow::with(['book', 'user'])->where('user_id', $user_id)->get();
        // dd($borrowedBooks);
        return view('layouts.borrow.borrow', compact('borrowedBooks'));
    }

    public function destroy($id)
    {
        $borrowedBooks = Borrow::find($id);
        $borrowedBooks->delete();
        return redirect('/borrow');
    }
}
