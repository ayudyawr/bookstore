<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Buku extends Model
{
    use HasFactory;
    protected $table = "books";
    protected $fillable = ["title", "writer", "publisher", "synopsis", "year", "genre_id"];

    public function genre()
    {
        return $this->belongsTo(Genre::class);
    }

    public function borrow()
    {
        return $this->hasMany(Borrow::class);
    }
}
